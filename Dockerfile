FROM kasmweb/core-ubuntu-bionic:1.10.0
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########

# Update

RUN apt update -y && apt upgrade -y

# Install JDK
RUN apt install default-jre -y

# Install wget

RUN apt install wget -y

# Install desktop-file-utils
RUN apt install desktop-file-utils -y

# Download eclipse tar
RUN wget https://eclipse.mirror.rafal.ca/oomph/epp/2021-12/R/eclipse-inst-jre-linux64.tar.gz


# Extrace tar file
RUN tar -xvzf /home/kasm-default-profile/eclipse-inst-jre-linux64.tar.gz 

# Remove tar file
RUN rm /home/kasm-default-profile/eclipse-inst-jre-linux64.tar.gz


# Create readme
RUN echo "In terminal type .//eclipse-installer/eclipse-inst" >> /home/kasm-default-profile/Desktop/Readme.txt



######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000

